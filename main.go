package main

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"log"
	"net/http"

	"github.com/gorilla/mux"
)

type pedestrian struct {
	ID     string `json:"pid,omitempty"`
	Name   string `json:"name"`
	Age    uint8  `json:"age"`
	Gender string `json:"gender"`
}

var pedestrians = []pedestrian{
	{
		ID:     "ABC",
		Name:   "Paco",
		Age:    20,
		Gender: "Male",
	},
}

func get(w http.ResponseWriter, r *http.Request) {
	pid := mux.Vars(r)["pid"]
	w.Header().Set("Content-Type", "application/json")

	found := false
	for _, pedestrian := range pedestrians {
		if pedestrian.ID == pid {
			found = true
			json.NewEncoder(w).Encode(pedestrian)
		}
	}

	if !found {
		w.WriteHeader(http.StatusNotFound)
	}
}

func update(w http.ResponseWriter, r *http.Request) {
	w.Header().Set("Content-Type", "application/json")
	pid := mux.Vars(r)["pid"]
	var updatedPedestrian pedestrian
	updatedPedestrian.ID = pid

	reqBody, err := ioutil.ReadAll(r.Body)
	if err != nil {
		fmt.Fprintf(w, "Error reading a pedestrian JSON object")
	}
	json.Unmarshal(reqBody, &updatedPedestrian)
	found := false
	for i, currentPed := range pedestrians {
		if currentPed.ID == pid {
			currentPed.Name = updatedPedestrian.Name
			currentPed.Age = updatedPedestrian.Age
			currentPed.Gender = updatedPedestrian.Gender
			pedestrians = append(pedestrians[:i], currentPed)
			json.NewEncoder(w).Encode(currentPed)
			found = true
		}
	}

	if !found {
		pedestrians = append(pedestrians, updatedPedestrian)
		json.NewEncoder(w).Encode(updatedPedestrian)
	}

}

func getAll(w http.ResponseWriter, r *http.Request) {
	w.Header().Set("Content-Type", "application/json")
	json.NewEncoder(w).Encode(pedestrians)
}

func main() {
	router := mux.NewRouter().StrictSlash(true)
	router.HandleFunc("/", func(w http.ResponseWriter, r *http.Request) {
		fmt.Fprintf(w, "Welcome home!")
	})
	router.HandleFunc("/pedestrians", getAll).Methods("GET")
	router.HandleFunc("/pedestrians/{pid}", update).Methods("PATCH")
	router.HandleFunc("/pedestrians/{pid}", get).Methods("GET")
	log.Fatal(http.ListenAndServe(":4004", router))
}
